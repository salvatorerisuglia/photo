---
title: "Foto"
date: 2018-12-20T09:55:59+01:00
images: ["sr.jpg"]
draft: false
---

ecco alcune foto:

![John's_Lane_Church_Exterior,_Dublin,_Ireland_-_Diliff](John's_Lane_Church_Exterior,_Dublin,_Ireland_-_Diliff.jpg)

![Balangiga bells on display during repatriation ceremony at Villamor Air Base](Balangiga_bells.jpg)

![Tower_42_looking_north_from_Bishopsgate_2011-05-04](Tower_42_looking_north_from_Bishopsgate_2011-05-04.jpg)
